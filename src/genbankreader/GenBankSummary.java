/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genbankreader;

/**
 *
 * @author ahclugtenberg
 */
public class GenBankSummary {
    private final String genBankFile;
    private ExtractInformationGenBank data;
    
    public GenBankSummary(String genBankFile) {
        this.genBankFile = genBankFile;  
        getSummaryInfo();
    }
    
    private void getSummaryInfo() {
        this.data = new ExtractInformationGenBank(this.genBankFile);
        this.data.initializeSummary();
    }
    
    private String getFileName() {       
        //*Windows PCs use backslash*//
        if (this.genBankFile.contains("\\")) {
            String[] parts = this.genBankFile.split("\\\\");
            return parts[parts.length-1];
        } else if (this.genBankFile.contains("/")) {
            String[] parts = this.genBankFile.split("/");
            return parts[parts.length-1];
        }
            return this.genBankFile;
        }
    
    public void giveSummary() {
        System.out.println("file              " + getFileName());
        System.out.println("organism          " + this.data.getOrganism());
        System.out.println("accession         " + this.data.getAccession());
        System.out.println("sequence length   " + this.data.getSequenceLength());
        System.out.println("number of genes   " + this.data.getGeneAmount());
        
        try {
            System.out.println("gene F/R balance  " + this.data.getGeneAmountForwardStrand() / this.data.getGeneAmount());
        } catch (ArithmeticException ae) {
            System.out.println("ArithmeticException by " + ae);
        }
        
        System.out.println("number of CDSs    " + this.data.getAmountCDS());
    }
} 
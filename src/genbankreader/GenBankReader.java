package genbankreader;

import java.util.Arrays;

public final class GenBankReader {
    /**
     * private constructor because this class is not supposed to be instantiated.
     */
    private GenBankReader() {
    }
    public static void main(final String[] args) {
        try {
            GenBankCliOptionsProvider op = new GenBankCliOptionsProvider(args);

        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
        }
    }
}


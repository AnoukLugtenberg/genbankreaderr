/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genbankreader;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractInformationGenBank {
    private final String genBankFile;
    private String data;
    private String definition = "";
    private String accession = "";
    private String organism = "";
    private int sequenceLength = 0;
    private int geneAmount;
    private int geneAmountForwardStrand;
    private int amountCDS;
    private String dnaSeq;
    private Set<String> startStop;
    
    public ExtractInformationGenBank(String genBankFile) {
        this.genBankFile = genBankFile;
    }
    
    public void initializeSummary() {
        openFile();
        setDefinition();
        setAccession();
        setOrganism();
        setSequenceLength();
        setGeneAmount();
        setGeneAmountForwardStrand();
        setAmountCDS();
    }
    
    public void initializeFetchGene(String gene) {
        openFile();
        setDnaSeq();
        setGeneStartStop(gene);
        getGeneStartStop();
    }
    
    public void initializeCDS(String gene) {
        openFile();
        setProteinSeq();
    }
    
    private void openFile() {       
        OpenFile gb = new OpenFile(genBankFile);
        try {
            this.data = gb.readFile();
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        }
    }
    
    private void setDefinition() {
        Pattern pattern = Pattern.compile(" *DEFINITION  *([^.]*)");
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            this.definition = matcher.group(1);
        }
    }
    
    public String getDefinition() {
        return this.definition;
    }
    
    private void setAccession() {
        Pattern pattern = Pattern.compile("ACCESSION *(.*)");
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            this.accession = matcher.group(1);
        }      
    }
    
    public String getAccession() {
        return this.accession;
    }
    
    private void setOrganism() {
        Pattern pattern = Pattern.compile(" *ORGANISM *(.*)");
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            this.organism = matcher.group(1);
        }
    }
    
    public String getOrganism() {
        return this.organism;
    }
    
    private void setSequenceLength() {
        Pattern pattern = Pattern.compile(" *LOCUS.* ((\\d+) bp)");
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            this.sequenceLength = Integer.parseInt(matcher.group(2));
        }
    }
    
    public int getSequenceLength() {
        return this.sequenceLength;
    }
    
    private void setGeneAmount() {
        Pattern pattern = Pattern.compile(" {2,}gene {2,}");
        Matcher matcher = pattern.matcher(this.data);
        int i = 0;
        while (matcher.find()) {
            i++;
        }
        this.geneAmount = i;       
    }
    
    public int getGeneAmount() {
        return this.geneAmount;
    }
    
    private void setGeneAmountForwardStrand() {
        Pattern pattern = Pattern.compile(" {2,}gene {2,}complement");
        Matcher matcher = pattern.matcher(this.data);
        int i = 0;
        while (matcher.find()) {
            i++;
        }
        this.geneAmountForwardStrand = i;
    }
    
    public int getGeneAmountForwardStrand() {
        return this.geneAmountForwardStrand;
    }
    
    private void setAmountCDS() {
        Pattern pattern = Pattern.compile(" {2,}CDS {2,}");
        Matcher matcher = pattern.matcher(this.data);
        int i = 0;
        while (matcher.find()) {
            i++;
        }
        this.amountCDS = i;
    }
    
    public int getAmountCDS() {
        return this.amountCDS;
    }  
    
    public void setDnaSeq() {
        Pattern pattern = Pattern.compile(" *ORIGIN *\\n *((\\d* * \\w*\\n*)*)");
        Matcher matcher = pattern.matcher(this.data);
        if (matcher.find()) {
            String seq = matcher.group(1).replaceAll("[ \\d\\n]", "");
            this.dnaSeq = seq;
        }
    }
    
    public String getDnaSeq() {
        return this.dnaSeq;
    }
    
    public void setGeneStartStop(String gene) {
        //*Misschien nog iets met complement doen..?*//
        Set<String> ss = new LinkedHashSet<>();
        Pattern pattern = Pattern.compile("\\w*?\\(?(\\d*..\\d*)\\)?\\n *(?:\\/gene=|\\/locus_tag=)\"" + gene + "\"");
        Matcher matcher = pattern.matcher(this.data);
        while (matcher.find()) {
            ss.add(matcher.group(1));
        }
        this.startStop = ss;
    }
    
    public Set<String> getGeneStartStop() {
        return this.startStop;
    }
    
    public void setProteinSeq() {
        Set<String> protein = new LinkedHashSet<>();
        Pattern pattern = pattern.compile("");
        Matcher matcher = pattern.matcher(this.data);
        while (matcher.find()) {
            protein.add(matcher.group());
        }
    }
    
    public static void main (String[] args) {
    ExtractInformationGenBank gb = new ExtractInformationGenBank("/homes/ahclugtenberg/NetBeansProjects/genbankreader/example_genbank_file.gb");
    //ExtractInformationGenBank gb = new ExtractInformationGenBank("C:\\Users\\koen\\Documents\\NetBeansProjects\\GenBankReader\\example_genbank_file.gb");
}
}

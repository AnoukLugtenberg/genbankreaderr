/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genbankreader;

import java.util.ArrayList;

/**
 *
 * @author ahclugtenberg
 */
public class GenBankFetchGene {
    private final String genBankFile;
    private final String geneName;
    private ExtractInformationGenBank data;
    private String dnaSeq;
    
    public GenBankFetchGene(String genBankFile, String geneName) {
        this.genBankFile = genBankFile;
        this.geneName = geneName;
    }
    
    public void initialize() {
        getGeneInformation();
        getDnaSeq();
    }
    
    private void getGeneInformation() {
        this.data = new ExtractInformationGenBank(this.genBankFile);
        this.data.initializeFetchGene(this.geneName);
    }
    
    private void getDnaSeq() {
        ArrayList <String> dna = new ArrayList<>();
        for (String i : this.data.getGeneStartStop()) {
            String[] parts = i.split("\\.\\.");
            //Convert string to int
            int start = Integer.parseInt(parts[0])-1;
            int stop = Integer.parseInt(parts[1]);
            
            this.dnaSeq = this.data.getDnaSeq().substring(start, stop);
            dna.add(dnaSeq);
        }
     
        for (String i : dna) {
            String parsedStr = i.replaceAll("(.{80})", "$1\n");
            System.out.println(">gene " + this.geneName + " sequence\n" + parsedStr);
            
        }
    }
    

   

    
    
//    public static void main (String[] args) {
//    GenBankFetchGene gb = new GenBankFetchGene("/homes/ahclugtenberg/NetBeansProjects/genbankreader/example_genbank_file.gb", "AXL2");
//    //ExtractInformationGenBank gb = new ExtractInformationGenBank("C:\\Users\\koen\\Documents\\NetBeansProjects\\GenBankReader\\example_genbank_file.gb");
//    }
}


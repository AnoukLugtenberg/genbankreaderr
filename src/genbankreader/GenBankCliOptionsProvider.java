/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genbankreader;

import java.io.File;
import java.io.IOException;
import org.apache.commons.cli.*;

public class GenBankCliOptionsProvider { 
    private static final String HELP = "help";   
    private static final String INFILE = "infile";
    private static final String SUMMARY = "summary";
    private static final String FETCH_GENE = "fetch_gene";
    private static final String FETCH_CDS = "fetch_cds";
    
    private Options options;
    private CommandLine commandLine;
    private final String[] args;
    
    public GenBankCliOptionsProvider(final String[] args) {
        this.args = args;
        initialize();
    }
    
    private void initialize() {
        buildOptions();
        try {
            processCommandLine();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }    
    
    private void buildOptions() {
        //* create Options object *//
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints this message");       
        Option summaryOption = new Option("s", SUMMARY, false, "Returns a textual summary of the parsed file: parsed file, "
                + "length of the sequence, for genes: count and forward/reverse balance and for CS features: count only");
        Option fetchGeneOption = new Option("fg", FETCH_GENE, true, "Returns nucleotide sequences of the genes that match the gene name pattern, "
                + "in Fasta format. --fetch_gene <PATTERN>");       
        Option fileOption = new Option("f", INFILE, true, "The GenBank file to be parsed"); 
        Option fetchCDSOption = new Option("fc", FETCH_CDS, true, "Returns protein sequence of the genes that match the gene name pattern");
        
        //*Adds options*//
        options.addOption(helpOption);
        options.addOption(fileOption);
        options.addOption(summaryOption);
        options.addOption(fetchGeneOption);

    }
      
    private void processCommandLine() throws IOException {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.args);
            if (commandLine.hasOption(HELP)) {
                printHelp();
            }
            
            String inputFile = commandLine.getOptionValue(INFILE);        
            if (inputFile == null) {
                throw new IOException("No input file specified");       
            } else {
                //*Checks if file exists and throws exception if not*//
                File file = new File(inputFile);           
                if (!file.exists()) {
                    throw new IOException("input file [" + inputFile + "] not found");
                }
                
                //*Checks if file is a genbank file*//
                String ext = inputFile.substring(inputFile.lastIndexOf(".") + 1, inputFile.length());
                String gb = "gb";
                if (!ext.equals(gb)) {
                    throw new IOException("Provide a genbank file");
                }
                
                if (commandLine.hasOption(SUMMARY)) {
                    GenBankSummary summary = new GenBankSummary(inputFile);
                    summary.giveSummary();
                }
                
                String geneName = commandLine.getOptionValue(FETCH_GENE);
                if (commandLine.hasOption(FETCH_GENE)) {
                    GenBankFetchGene fetchGene = new GenBankFetchGene(inputFile, geneName);
                    fetchGene.initialize();
                }
            }        
        }
        catch(ParseException e) {
            throw new IllegalStateException(e);
        }
    }

    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("A parser for files in GenBank format", options);
    }
}

    
    
    
    

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package genbankreader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class OpenFile {
    private final String genBankFile;
    
    public OpenFile(String genBankFile) {
        this.genBankFile = genBankFile;
    }
    
    public String readFile() throws IOException {
            byte[] encoded = Files.readAllBytes(Paths.get(this.genBankFile));
            return new String(encoded, "utf-8");       
    }
}



